﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;

namespace divisasmovil.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Atributos
        private decimal dolar;
        private decimal euro;
        private decimal libra;
        #endregion

        #region Propiedades
        public decimal Pesos { get; set; }
        public decimal Dolar {
            set {
                if (dolar != value)
                {
                    dolar = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dolar"));
                }
            }
            get
            {
                return dolar;
            }
        }
        public decimal Euro {
            set
            {
                if (euro != value)
                {
                    euro = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Euro"));
                }
            }
            get
            {
                return euro;
            }
        }
        public decimal Libra {
            set
            {
                if (libra != value)
                {
                    libra = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Libra"));
                }
            }
            get
            {
                return libra;
            }
        } 
        #endregion

        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Comandos
        public ICommand ConverterCommand { get { return new RelayCommand(ConvertMoney); } }
        private void ConvertMoney()
        {
            if (Pesos <= 0)
            {
                App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un valor mayor a cero", "aceptar");
                return;
            }

            Dolar = Pesos / 19.0291336M;
            Euro = Pesos / 20.5847653M;
            Libra = Pesos / 23.6225762M;
        } 
        #endregion
    }
}
